import { Component, OnInit } from '@angular/core';
import { HackernewsService } from '../hackernews.service';
import { HackerNewsItem } from '../HackerNewsItem';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public items: Array<HackerNewsItem> = [];
  public isLoading = true;

  constructor(private hackernewsService: HackernewsService) {}

  ngOnInit(): void {
    this.hackernewsService.getContentFromTopStories().subscribe(items => {
      this.items = items;
      this.isLoading = false;
    },
    (error) => {
      this.isLoading = false;
      console.log('Could not load latest articles', error);
    });
  }

  public doRefresh(event) {
    this.hackernewsService.getContentFromTopStories().subscribe(items => {
      this.items = items;
      event.target.complete();
    });
  }

  public goToPost(url: string) {
    window.location.href = url;
  }

}
