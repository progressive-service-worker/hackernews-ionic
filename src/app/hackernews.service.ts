import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { mergeMap, combineAll, } from 'rxjs/operators';
import { HackerNewsItem } from './HackerNewsItem';

@Injectable({
  providedIn: 'root'
})
export class HackernewsService {
  private hackerNewsBaseUrl = 'https://hacker-news.firebaseio.com/v0/';

  constructor(private http: HttpClient) { }

  public getContentFromTopStories(): Observable<HackerNewsItem[]> {
    return this.getTopStories()
      .pipe(
        mergeMap(items => {
          return items.map(item => this.getItem(item));
        }, undefined, 10),
      ).pipe(combineAll());
  }

  private getTopStoriesUrl(): string {
    return this.hackerNewsBaseUrl + 'topstories.json';
  }

  private getItemUrl(id: number): string {
    return this.hackerNewsBaseUrl + `item/${id}.json`;
  }

  private getTopStories(): Observable<Array<number>> {
    return this.http.get<number[]>(this.getTopStoriesUrl());
  }

  private getItem(id: number): Observable<HackerNewsItem> {
    return this.http.get<HackerNewsItem>(this.getItemUrl(id));
  }
}
